Module 104 - Bertolini Flavio (1C)
---

## Explication globale du projet :
L'application était initialement prévue pour effectuer des tests entres des étudiants de différentes structures, dans différentes branches. 

En raison du temps et du manque d'apprentissage préalable au travail, l'application rendue permettra les choses suivantes : 
* Ajout, édition, suppression et effacement d'utilisateurs
* Ajout, édition, suppression et effacement de centres de formation
* Ajout, édition, suppression et effacement de branches
* Assignation et désassignation de branches en relation avec un élève

L'application servira donc à savoir quelles branches suivent quels élèves. 


## Utilisation
Il faut avoir installé wamp ou un autre server web et faire en sorte que le login de phpmyadmin soit root et le mot de passe vide. 
Lancez wamp. 
Importez la base de donnée avec le fichier APP_FILMS/zzzdemos/1_ImportationDumpSql.py. 
Dans PyCharm lancez 1_run_server_flask.py en faisant clic droit sur l'onglet puis Run. 

## Environnement de test
Affin d'accéder à toutes les fonctionnalités de l'application, il faudra utiliser les identifiants de connexion suivants : 
* Login : netanonima
* Mot de passe : Test1234 (premier T en majuscule)

### Informations supplémentaires

* Pour la table d'utilisateurs (login) les boutons n'ont été activés sur la liste d'affichage que par un administrateur (droit supérieur à 1). 
Les utilisateurs non administrateurs utilisent le bouton "enregistrement" pour le Create et seul un administrateur peut éditer un utilisateur et/ou modifier son mot de passe.
Dans l'édition d'un utilisateur si les champs de mot de passe ne sont pas renseignés, ces derniers ne sont pas mis à jour.
