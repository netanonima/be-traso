
from flask_wtf import FlaskForm
from wtforms import SelectField
from wtforms import StringField
from wtforms import SubmitField
from wtforms import PasswordField
from wtforms import validators
from wtforms.validators import DataRequired
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterLogin(FlaskForm):
    id_structure_wtf = SelectField('Centre de formation (liste déroulante)',
                                   validators=[DataRequired(message="Sélectionner un centre de formation.")],
                                   validate_choice=False
                                   )

    nom_utilisateur_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    nom_utilisateur_wtf = StringField("Saisir le nom d'utilisateur ",
                                      validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                  Regexp(nom_utilisateur_regexp,
                                                         message="Pas de chiffres, de caractères "
                                                                 "spéciaux, "
                                                                 "d'espace à double, de double "
                                                                 "apostrophe, de double trait union")
                                                  ])
    nom_mdp_regexp = "^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$"
    nom_mdp_wtf = PasswordField("Saisir le mot de passe ", validators=[Length(min=8, max=20, message="min 8 max 20"),
                                                                       validators.DataRequired(),
                                                                       validators.EqualTo('nom_mdp2_wtf',
                                                                                          message='Les mots de passe saisis doivent être indentiques'),
                                                                       Regexp(nom_mdp_regexp,
                                                                              message="Au moins 8 caractères, "
                                                                                      "1 lettre et 1 chiffre")
                                                                       ])

    nom_mdp2_regexp = "^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$"
    nom_mdp2_wtf = PasswordField("Saisir le mot de passe à nouveau ",
                                 validators=[Length(min=8, max=20, message="min 8 max 20"),
                                             Regexp(nom_mdp2_regexp,
                                                    message="Au moins 8 caractères, "
                                                            "1 lettre et 1 chiffre")
                                             ])
    nom_courriel_regexp = "^(\w|\.|\_|\-)+[@](\w|\_|\-|\.)+[.]\w{2,3}$"
    nom_courriel_wtf = StringField("Saisir le courriel ", validators=[Length(min=2, max=40, message="min 2 max 40"),
                                                                      Regexp(nom_courriel_regexp,
                                                                             message="N'est pas un formail de courriel "
                                                                                     "(p.ex. : info@aaaa.com)")
                                                                      ])

    submit = SubmitField("Enregistrer l'utilisateur")


class FormWTFLogin(FlaskForm):

    identifiant_wtf = StringField("Saisir le nom d'utilisateur ")
    nom_mdp_wtf = PasswordField("Saisir le mot de passe ")

    submit = SubmitField("S'identifier")


class FormWTFUpdateLogin(FlaskForm):
    """
        Dans le formulaire "genre_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    id_structure_wtf_update = SelectField('Centre de formation (liste déroulante)',
                                          validators=[DataRequired(message="Sélectionner un centre de formation.")],
                                          validate_choice=False
                                          )

    nom_utilisateur_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    nom_utilisateur_wtf_update = StringField("Saisir le nom d'utilisateur ",
                                             validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                         Regexp(nom_utilisateur_regexp,
                                                                message="Pas de chiffres, de caractères "
                                                                        "spéciaux, "
                                                                        "d'espace à double, de double "
                                                                        "apostrophe, de double trait union")
                                                         ])
    nom_mdp_regexp = "^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$"
    nom_mdp_wtf_update = PasswordField("Saisir le mot de passe ",
                                       validators=[Length(min=8, max=20, message="min 8 max 20"),
                                                   validators.optional(),
                                                   validators.EqualTo('nom_mdp2_wtf_update',
                                                                      message='Les mots de passe saisis doivent être indentiques'),
                                                   Regexp(nom_mdp_regexp,
                                                          message="Au moins 8 caractères, "
                                                                  "1 lettre et 1 chiffre")
                                                   ])

    nom_mdp2_regexp = "^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$"
    nom_mdp2_wtf_update = PasswordField("Saisir le mot de passe à nouveau ",
                                        validators=[Length(min=8, max=20, message="min 8 max 20"),
                                                    validators.optional(),
                                                    Regexp(nom_mdp2_regexp,
                                                           message="Au moins 8 caractères, "
                                                                   "1 lettre et 1 chiffre")
                                                    ])
    nom_courriel_regexp = "^(\w|\.|\_|\-)+[@](\w|\_|\-|\.)+[.]\w{2,3}$"
    nom_courriel_wtf_update = StringField("Saisir le courriel ",
                                          validators=[Length(min=2, max=40, message="min 2 max 40"),
                                                      Regexp(nom_courriel_regexp,
                                                             message="N'est pas un formail de courriel "
                                                                     "(p.ex. : info@aaaa.com)")
                                                      ])

    submit = SubmitField("Mettre à jour utilisateur")


class FormWTFDeleteLogin(FlaskForm):
    """
        Dans le formulaire "genre_delete_wtf.html"

        nom_genre_delete_wtf : Champ qui reçoit la valeur du genre, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "genre".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_genre".
    """
    nom_login_delete_wtf = StringField("Effacer cet utilisateur")
    submit_btn_del = SubmitField("Effacer utilisateur")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
