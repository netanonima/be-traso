"""
    Fichier : gestion_login_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les login.
"""
import re
import sys

import flask_argon2
import pymysql
from flask import flash, Flask
from flask import render_template
from flask import request
from flask import url_for
from flask import redirect
from flask_argon2 import Argon2
from flask import session

from APP_FILMS.erreurs.msg_erreurs import error_codes

app = Flask(__name__)
argon2 = Argon2(app)

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.erreurs.msg_erreurs import *
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.login.gestion_login_wtf_forms import FormWTFAjouterLogin
from APP_FILMS.login.gestion_login_wtf_forms import FormWTFDeleteLogin
from APP_FILMS.login.gestion_login_wtf_forms import FormWTFUpdateLogin
from APP_FILMS.login.gestion_login_wtf_forms import FormWTFLogin
"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /login_afficher
    
    Test : ex : http://127.0.0.1:5005/login_afficher
    
    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                id_login_sel = 0 >> tous les login.
                id_login_sel = "n" affiche le login dont l'id est "n"
"""


@obj_mon_application.route("/login_ajouter", methods=['GET', 'POST'])
def login_ajouter_wtf():
    form = FormWTFAjouterLogin()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion login ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionLogin {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                structure_id_wtf = form.id_structure_wtf.data
                name_utilisateur_wtf = form.nom_utilisateur_wtf.data
                name_mdp_wtf = form.nom_mdp_wtf.data
                name_courriel_wtf = form.nom_courriel_wtf.data

                structure_id = structure_id_wtf
                name_utilisateur = name_utilisateur_wtf.lower()
                name_courriel = name_courriel_wtf.lower()
                valeurs_insertion_dictionnaire = {"value_structure_id": structure_id,
                                                  "value_intitule_utilisateur": name_utilisateur,
                                                  "value_intitule_mdp": name_mdp_wtf,
                                                  "value_intitule_courriel": name_courriel}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_login = """INSERT INTO t_login (username, password, email, fk_structure_id, grade) VALUES (%(value_intitule_utilisateur)s,%(value_intitule_mdp)s,%(value_intitule_courriel)s,%(value_structure_id)s,1)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_login, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('login_subject_afficher', order_by='DESC', id_login_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_login_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_login_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion login CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")
    # if request.method == "GET":
    with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
        strsql_login_afficher = """SELECT id_structure, name_structure FROM t_structure ORDER BY name_structure ASC"""
        mc_afficher.execute(strsql_login_afficher)

    data_login = mc_afficher.fetchall()
    print("demo_select_wtf data_login ", data_login, " Type : ", type(data_login))

    login_val_list_dropdown = []
    for i in data_login:
        login_val_list_dropdown.append((i['id_structure'], i['name_structure']))

    form.id_structure_wtf.choices = login_val_list_dropdown
    session['login_val_list_dropdown'] = login_val_list_dropdown

    return render_template("login/login_ajouter_wtf.html", form=form)


@obj_mon_application.route("/login", methods=['GET', 'POST'])
def login():
    form = FormWTFLogin()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion login ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionLogin {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                identifiant_wtf = form.identifiant_wtf.data
                motDePasse_wtf = form.nom_mdp_wtf.data

                name_utilisateur = identifiant_wtf.lower()
                valeurs_insertion_dictionnaire = {"value_identifiant": identifiant_wtf, "value_mdp": motDePasse_wtf}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                    strsql_login_afficher = """SELECT username, password, grade FROM t_login WHERE username = %(value_identifiant)s"""
                    mc_afficher.execute(strsql_login_afficher, valeurs_insertion_dictionnaire)
                data_login = mc_afficher.fetchall()

                for i in data_login:
                    data_login_mdp = (i['password'])
                    data_login_username = (i['username'])
                    data_login_grade = (i['grade'])

                if not data_login:
                    # le nom d'utilisateur n'existe pas
                    print("le nom d'utilisateur n'existe pas")
                    flash(f"Cet utilisateur n'existe pas. Prière de recommencer", "danger")
                else:
                    # le nom d'utilisateur existe
                    # test si le mot de passe est ok
                    if argon2.check_password_hash(data_login_mdp, motDePasse_wtf):
                        # le mot de passe est le bon
                        # création de session
                        print("utilisateur authentifié")
                        session['logged_in'] = True
                        session['username'] = data_login_username
                        session['grade'] = data_login_grade
                        flash(f"Authentification exécutée avec succès. ", "success")
                        return redirect(url_for('login_subject_afficher', order_by='DESC', id_login_sel=0))
                    else:
                        # mauvais mot de passe
                        print("mauvais mot de passe saisi pour utilisateur existant")
                        flash(f"Le mot de passe saisi pour l'utilisateur n'est pas exact. Prière de recommencer",
                              "danger")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                # return redirect(url_for('login_afficher', order_by='DESC', id_login_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_login_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_login_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion login CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("login/login.html", form=form)


@obj_mon_application.route("/logout", methods=['GET', 'POST'])
def logout():
    session['logged_in'] = False
    session['username'] = ""
    session['grade'] = ""
    flash(f"Déconnexion exécutée avec succès. ", "success")
    return redirect(url_for('login_subject_afficher', order_by='DESC', id_login_sel=0))


@obj_mon_application.route("/login_update", methods=['GET', 'POST'])
def login_update_wtf():
    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_login"
    id_login_update = request.values['id_login_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateLogin()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "login_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            structure_id_wtf = form_update.id_structure_wtf_update.data

            name_utilisateur_wtf = form_update.nom_utilisateur_wtf_update.data
            name_courriel_wtf = form_update.nom_courriel_wtf_update.data
            name_mdp_wtf = form_update.nom_mdp_wtf_update.data

            if not len(form_update.nom_mdp_wtf_update.data) == 0:
                name_mdp_wtf_update = argon2.generate_password_hash(name_mdp_wtf)
            else:
                name_mdp_wtf_update = name_mdp_wtf

            structure_id = structure_id_wtf
            name_utilisateur = name_utilisateur_wtf.lower()
            name_courriel = name_courriel_wtf.lower()
            valeur_update_dictionnaire = {"value_login_id": id_login_update,
                                          "value_structure_id": structure_id,
                                          "value_intitule_utilisateur": name_utilisateur,
                                          "value_intitule_mdp": name_mdp_wtf_update,
                                          "value_intitule_courriel": name_courriel}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            if not len(form_update.nom_mdp_wtf_update.data) == 0:
                str_sql_update_intitulelogin = """UPDATE t_login SET fk_structure_id = %(value_structure_id)s, username = %(value_intitule_utilisateur)s, password = %(value_intitule_mdp)s,email = %(value_intitule_courriel)s WHERE id_login = %(value_login_id)s"""
            else:
                str_sql_update_intitulelogin = """UPDATE t_login SET fk_structure_id = %(value_structure_id)s, username = %(value_intitule_utilisateur)s, email = %(value_intitule_courriel)s WHERE id_login = %(value_login_id)s"""

            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intitulelogin, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_login_update"
            return redirect(url_for('login_subject_afficher', order_by="ASC", id_login_sel=id_login_update))
        elif request.method == "GET":

            #### partie pour l'affichage du selectField
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_login_afficher = """SELECT id_structure, name_structure FROM t_structure ORDER BY name_structure ASC"""
                mc_afficher.execute(strsql_login_afficher)

            data_login = mc_afficher.fetchall()

            login_val_list_dropdown_update = []
            for i in data_login:
                login_val_list_dropdown_update.append((i['id_structure'], i['name_structure']))

            form_update.id_structure_wtf_update.choices = login_val_list_dropdown_update
            session['login_val_list_dropdown_update'] = login_val_list_dropdown_update

            #### fin de la partie pour l'affichage du selectField

            # Opération sur la BD pour récupérer "id_login" et "username" de la "t_login"
            str_sql_id_login = "SELECT id_login, fk_structure_id, username, email FROM t_login WHERE id_login = %(value_id_login)s"
            valeur_select_dictionnaire = {"value_id_login": id_login_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_login, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom login" pour l'UPDATE
            data_login = mybd_curseur.fetchone()

            # Afficher la valeur sélectionnée dans le champ du formulaire "login_update_wtf.html"
            form_update.id_structure_wtf_update.data = data_login["fk_structure_id"]
            form_update.nom_utilisateur_wtf_update.data = data_login["username"]
            form_update.nom_courriel_wtf_update.data = data_login["email"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans login_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans login_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans login_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans login_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("login/login_update_wtf.html", form_update=form_update)


@obj_mon_application.route("/login_delete", methods=['GET', 'POST'])
def login_delete_wtf():
    data_subjects_attribue_login_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_login"
    id_login_delete = request.values['id_login_btn_delete_html']

    # Objet formulaire pour effacer le login sélectionné.
    form_delete = FormWTFDeleteLogin()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("login_subject_afficher", order_by="ASC", id_login_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "login/login_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_subjects_attribue_login_delete = session['data_subjects_attribue_login_delete']
                print("data_subjects_attribue_login_delete ", data_subjects_attribue_login_delete)

                flash(f"Effacer le login de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer login" qui va irrémédiablement EFFACER le login
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_login": id_login_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_subjects_login = """DELETE FROM t_login_subject WHERE fk_login_id = %(value_id_login)s"""
                str_sql_delete_idlogin = """DELETE FROM t_login WHERE id_login = %(value_id_login)s"""
                # Manière brutale d'effacer d'abord la "fk_login", même si elle n'existe pas dans la "t_login_subject"
                # Ensuite on peut effacer le login vu qu'il n'est plus "lié" (INNODB) dans la "t_login_subject"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_subjects_login, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_idlogin, valeur_delete_dictionnaire)

                flash(f"Utilisateur définitivement effacé !!", "success")
                print(f"Utilisateur définitivement effacé !!")

                # afficher les données
                return redirect(url_for('login_subject_afficher', order_by="ASC", id_login_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_login": id_login_delete}
            print(id_login_delete, type(id_login_delete))

            # Requête qui affiche tous les subjects qui ont le login que l'utilisateur veut effacer
            str_sql_login_subjects_delete = """SELECT * FROM t_login_subject, t_subject 
                                             WHERE t_subject.id_subject = t_login_subject.fk_subject_id
                                            AND t_login_subject.fk_login_id = %(value_id_login)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_login_subjects_delete, valeur_select_dictionnaire)
            data_subjects_attribue_login_delete = mybd_curseur.fetchall()
            print("data_subjects_attribue_login_delete...", data_subjects_attribue_login_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "login/login_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_subjects_attribue_login_delete'] = data_subjects_attribue_login_delete

            # Opération sur la BD pour récupérer "id_login" et "username" de la "t_login"
            str_sql_id_login = "SELECT * FROM t_login WHERE id_login = %(value_id_login)s"

            mybd_curseur.execute(str_sql_id_login, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom login" pour l'action DELETE
            data_nom_login = mybd_curseur.fetchone()
            print("data_nom_login ", data_nom_login, " type ", type(data_nom_login), " login ",
                  data_nom_login["username"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "login_delete_wtf.html"
            form_delete.nom_login_delete_wtf.data = data_nom_login["username"]

            # Le bouton pour l'action "DELETE" dans le form. "login_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans login_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans login_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans login_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans login_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("login/login_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_subjects_associes=data_subjects_attribue_login_delete)



