"""
    Fichier : gestion_login_subject_crud.py
    Auteur : OM 2021.05.01
    Gestions des "routes" FLASK et des données pour l'association entre les login et les subject.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.erreurs.msg_erreurs import *
from APP_FILMS.login.gestion_login_wtf_forms import *
from APP_FILMS.login.gestion_login_crud import *

"""
    Nom : login_subject_afficher
    Auteur : OM 2021.05.01
    Définition d'une "route" /login_subject_afficher
    
    But : Afficher les login avec les subject associés pour chaque login.
    
    Paramètres : id_subject_sel = 0 >> tous les login.
                 id_subject_sel = "n" affiche le login dont l'id est "n"
                 
"""


@obj_mon_application.route("/login_subject_afficher/<int:id_login_sel>", methods=['GET', 'POST'])
def login_subject_afficher(id_login_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as Exception_init_login_subject_afficher:
                code, msg = Exception_init_login_subject_afficher.args
                flash(f"{error_codes.get(code, msg)} ", "danger")
                flash(f"Exception _init_login_subject_afficher problème de connexion BD : {sys.exc_info()[0]} "
                      f"{Exception_init_login_subject_afficher.args[0]} , "
                      f"{Exception_init_login_subject_afficher}", "danger")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {msg_erreurs.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_login_subject_afficher_data = """SELECT id_login, username, email, grade, 
                                                                                            GROUP_CONCAT(name_subject SEPARATOR ', ') as SubjectLogin FROM t_login_subject
                                                                                            RIGHT JOIN t_login ON t_login.id_login = t_login_subject.fk_login_id
                                                                                            LEFT JOIN t_subject ON t_subject.id_subject = t_login_subject.fk_subject_id
                                                                                            GROUP BY username"""

                if id_login_sel == 0:
                    # le paramètre 0 permet d'afficher tous les login
                    # Sinon le paramètre représente la valeur de l'id du login
                    mc_afficher.execute(strsql_login_subject_afficher_data)
                else:
                    # Constitution d'un dictionnaire pour associer l'id du login sélectionné avec un nom de variable
                    valeur_id_login_selected_dictionnaire = {"value_id_login_selected": id_login_sel}
                    # En MySql l'instruction HAVING fonctionne comme un WHERE... mais doit être associée à un GROUP BY
                    # L'opérateur += permet de concaténer une nouvelle valeur à la valeur de gauche préalablement définie.
                    strsql_login_subject_afficher_data += """ HAVING id_login= %(value_id_login_selected)s"""

                    mc_afficher.execute(strsql_login_subject_afficher_data, valeur_id_login_selected_dictionnaire)

                # Récupère les données de la requête.
                data_login_subject_afficher = mc_afficher.fetchall()
                print("data_subject ", data_login_subject_afficher, " Type : ", type(data_login_subject_afficher))

                # Différencier les messages.
                if not data_login_subject_afficher and id_login_sel == 0:
                    flash("""La table "t_login" est vide. !""", "warning")
                elif not data_login_subject_afficher and id_login_sel > 0:
                    # Si l'utilisateur change l'id_login dans l'URL et qu'il ne correspond à aucun login
                    flash(f"Le login {id_login_sel} demandé n'existe pas !!", "warning")
                # else:
                    # flash(f"Données login et subject affichés !!", "success")
        except Exception as Exception_login_subject_afficher:
            code, msg = Exception_login_subject_afficher.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception login_subject_afficher : {sys.exc_info()[0]} "
                  f"{Exception_login_subject_afficher.args[0]} , "
                  f"{Exception_login_subject_afficher}", "danger")

    # Envoie la page "HTML" au serveur.
    return render_template("login_subject/login_subject_afficher.html", data=data_login_subject_afficher)


"""
    nom: edit_login_subject_selected
    On obtient un objet "objet_dumpbd"

    Récupère la liste de tous les subject du login sélectionné par le bouton "MODIFIER" de "login_subject_afficher.html"
    
    Dans une liste déroulante particulière (tags-selector-tagselect), on voit :
    1) Tous les subject contenus dans la "t_subject".
    2) Les subject attribués au login selectionné.
    3) Les subject non-attribués au login sélectionné.

    On signale les erreurs importantes

"""


@obj_mon_application.route("/edit_login_subject_selected", methods=['GET', 'POST'])
def edit_login_subject_selected():
    if request.method == "GET":
        try:
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_subject_afficher = """SELECT id_subject, name_subject FROM t_subject ORDER BY id_subject ASC"""
                mc_afficher.execute(strsql_subject_afficher)
            data_subject_all = mc_afficher.fetchall()
            print("dans edit_login_subject_selected ---> data_subject_all", data_subject_all)

            # Récupère la valeur de "id_login" du formulaire html "login_subject_afficher.html"
            # l'utilisateur clique sur le bouton "Modifier" et on récupère la valeur de "id_login"
            # grâce à la variable "id_login_subject_edit_html" dans le fichier "login_subject_afficher.html"
            # href="{{ url_for('edit_login_subject_selected', id_login_subject_edit_html=row.id_login) }}"
            id_login_subject_edit = request.values['id_login_subject_edit_html']

            # Mémorise l'id du login dans une variable de session
            # (ici la sécurité de l'application n'est pas engagée)
            # il faut éviter de stocker des données sensibles dans des variables de sessions.
            session['session_id_login_subject_edit'] = id_login_subject_edit

            # Constitution d'un dictionnaire pour associer l'id du login sélectionné avec un nom de variable
            valeur_id_login_selected_dictionnaire = {"value_id_login_selected": id_login_subject_edit}

            # Récupère les données grâce à 3 requêtes MySql définie dans la fonction login_subject_afficher_data
            # 1) Sélection du login choisi
            # 2) Sélection des subject "déjà" attribués pour le login.
            # 3) Sélection des subject "pas encore" attribués pour le login choisi.
            # ATTENTION à l'ordre d'assignation des variables retournées par la fonction "login_subject_afficher_data"
            data_login_subject_selected, data_login_subject_non_attribues, data_login_subject_attribues = \
                login_subject_afficher_data(valeur_id_login_selected_dictionnaire)

            print(data_login_subject_selected)
            lst_data_login_selected = [item['id_login'] for item in data_login_subject_selected]
            print("lst_data_login_selected  ", lst_data_login_selected,
                  type(lst_data_login_selected))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les subject qui ne sont pas encore sélectionnés.
            lst_data_login_subject_non_attribues = [item['id_subject'] for item in data_login_subject_non_attribues]
            session['session_lst_data_login_subject_non_attribues'] = lst_data_login_subject_non_attribues
            print("lst_data_login_subject_non_attribues  ", lst_data_login_subject_non_attribues,
                  type(lst_data_login_subject_non_attribues))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les subject qui sont déjà sélectionnés.
            lst_data_login_subject_old_attribues = [item['id_subject'] for item in data_login_subject_attribues]
            session['session_lst_data_login_subject_old_attribues'] = lst_data_login_subject_old_attribues
            print("lst_data_login_subject_old_attribues  ", lst_data_login_subject_old_attribues,
                  type(lst_data_login_subject_old_attribues))

            print(" data data_login_subject_selected", data_login_subject_selected, "type ", type(data_login_subject_selected))
            print(" data data_login_subject_non_attribues ", data_login_subject_non_attribues, "type ",
                  type(data_login_subject_non_attribues))
            print(" data_login_subject_attribues ", data_login_subject_attribues, "type ",
                  type(data_login_subject_attribues))

            # Extrait les valeurs contenues dans la table "t_subject", colonne "name_subject"
            # Le composant javascript "tagify" pour afficher les tags n'a pas besoin de l'id_subject
            lst_data_login_subject_non_attribues = [item['name_subject'] for item in data_login_subject_non_attribues]
            print("lst_all_subject gf_edit_login_subject_selected ", lst_data_login_subject_non_attribues,
                  type(lst_data_login_subject_non_attribues))

        except Exception as Exception_edit_login_subject_selected:
            code, msg = Exception_edit_login_subject_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception edit_login_subject_selected : {sys.exc_info()[0]} "
                  f"{Exception_edit_login_subject_selected.args[0]} , "
                  f"{Exception_edit_login_subject_selected}", "danger")

    return render_template("login_subject/login_subject_modifier_tags_dropbox.html",
                           data_subject=data_subject_all,
                           data_login_subject_selected=data_login_subject_selected,
                           data_login_subject_attribues=data_login_subject_attribues,
                           data_login_subject_non_attribues=data_login_subject_non_attribues)


"""
    nom: update_login_subject_selected

    Récupère la liste de tous les subject du login sélectionné par le bouton "MODIFIER" de "login_subject_afficher.html"
    
    Dans une liste déroulante particulière (tags-selector-tagselect), on voit :
    1) Tous les subject contenus dans la "t_subject".
    2) Les subject attribués au login selectionné.
    3) Les subject non-attribués au login sélectionné.

    On signale les erreurs importantes
"""


@obj_mon_application.route("/update_login_subject_selected", methods=['GET', 'POST'])
def update_login_subject_selected():
    if request.method == "POST":
        try:
            # Récupère l'id du login sélectionné
            id_login_selected = session['session_id_login_subject_edit']
            print("session['session_id_login_subject_edit'] ", session['session_id_login_subject_edit'])

            # Récupère la liste des subject qui ne sont pas associés au login sélectionné.
            old_lst_data_login_subject_non_attribues = session['session_lst_data_login_subject_non_attribues']
            print("old_lst_data_login_subject_non_attribues ", old_lst_data_login_subject_non_attribues)

            # Récupère la liste des subject qui sont associés au login sélectionné.
            old_lst_data_login_subject_attribues = session['session_lst_data_login_subject_old_attribues']
            print("old_lst_data_login_subject_old_attribues ", old_lst_data_login_subject_attribues)

            # Effacer toutes les variables de session.
            session.clear()

            # Récupère ce que l'utilisateur veut modifier comme subject dans le composant "tags-selector-tagselect"
            # dans le fichier "login_subject_modifier_tags_dropbox.html"
            new_lst_str_login_subject = request.form.getlist('name_select_tags')
            print("new_lst_str_login_subject ", new_lst_str_login_subject)

            # OM 2021.05.02 Exemple : Dans "name_select_tags" il y a ['4','65','2']
            # On transforme en une liste de valeurs numériques. [4,65,2]
            new_lst_int_login_subject_old = list(map(int, new_lst_str_login_subject))
            print("new_lst_login_subject ", new_lst_int_login_subject_old, "type new_lst_login_subject ",
                  type(new_lst_int_login_subject_old))

            # Pour apprécier la facilité de la vie en Python... "les ensembles en Python"
            # https://fr.wikibooks.org/wiki/Programmation_Python/Ensembles
            # OM 2021.05.02 Une liste de "id_subject" qui doivent être effacés de la table intermédiaire "t_login_subject".
            lst_diff_subject_delete_b = list(
                set(old_lst_data_login_subject_attribues) - set(new_lst_int_login_subject_old))
            print("lst_diff_subject_delete_b ", lst_diff_subject_delete_b)

            # Une liste de "id_subject" qui doivent être ajoutés à la "t_login_subject"
            lst_diff_subject_insert_a = list(
                set(new_lst_int_login_subject_old) - set(old_lst_data_login_subject_attribues))
            print("lst_diff_subject_insert_a ", lst_diff_subject_insert_a)

            # SQL pour insérer une nouvelle association entre
            # "fk_login_id"/"id_login" et "fk_subject_id"/"id_subject" dans la "t_login_subject"
            strsql_insert_login_subject = """INSERT INTO t_login_subject (id_login_subject, fk_subject_id, fk_login_id)
                                                    VALUES (NULL, %(value_fk_subject_id)s, %(value_fk_login_id)s)"""

            # SQL pour effacer une (des) association(s) existantes entre "id_login" et "id_subject" dans la "t_login_subject"
            strsql_delete_login_subject = """DELETE FROM t_login_subject WHERE fk_subject_id = %(value_fk_subject_id)s AND fk_login_id = %(value_fk_login_id)s"""

            with MaBaseDeDonnee() as mconn_bd:
                # Pour le login sélectionné, parcourir la liste des subject à INSÉRER dans la "t_login_subject".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for id_subject_ins in lst_diff_subject_insert_a:
                    # Constitution d'un dictionnaire pour associer l'id du login sélectionné avec un nom de variable
                    # et "id_subject_ins" (l'id du subject dans la liste) associé à une variable.
                    valeurs_login_sel_subject_sel_dictionnaire = {"value_fk_login_id": id_login_selected,
                                                               "value_fk_subject_id": id_subject_ins}

                    mconn_bd.mabd_execute(strsql_insert_login_subject, valeurs_login_sel_subject_sel_dictionnaire)

                # Pour le login sélectionné, parcourir la liste des subject à EFFACER dans la "t_login_subject".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for id_subject_del in lst_diff_subject_delete_b:
                    # Constitution d'un dictionnaire pour associer l'id du login sélectionné avec un nom de variable
                    # et "id_subject_del" (l'id du subject dans la liste) associé à une variable.
                    valeurs_login_sel_subject_sel_dictionnaire = {"value_fk_login_id": id_login_selected,
                                                               "value_fk_subject_id": id_subject_del}

                    # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
                    # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
                    # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
                    # sera interprété, ainsi on fera automatiquement un commit
                    mconn_bd.mabd_execute(strsql_delete_login_subject, valeurs_login_sel_subject_sel_dictionnaire)

        except Exception as Exception_update_login_subject_selected:
            code, msg = Exception_update_login_subject_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception update_login_subject_selected : {sys.exc_info()[0]} "
                  f"{Exception_update_login_subject_selected.args[0]} , "
                  f"{Exception_update_login_subject_selected}", "danger")

    # Après cette mise à jour de la table intermédiaire "t_login_subject",
    # on affiche les login et le(urs) subject(s) associé(s).
    return redirect(url_for('login_subject_afficher', id_login_sel=id_login_selected))


"""
    nom: login_subject_afficher_data

    Récupère la liste de tous les subject du login sélectionné par le bouton "MODIFIER" de "login_subject_afficher.html"
    Nécessaire pour afficher tous les "TAGS" des subject, ainsi l'utilisateur voit les subject à disposition

    On signale les erreurs importantes
"""



def login_subject_afficher_data(valeur_id_login_selected_dict):
    print("valeur_id_login_selected_dict...", valeur_id_login_selected_dict)
    try:

        strsql_subject_selected = """SELECT id_login, username, email, grade, GROUP_CONCAT(id_subject) as SubjectLogin FROM t_login_subject
                                                        INNER JOIN t_login ON t_login.id_login = t_login_subject.fk_login_id
                                                        INNER JOIN t_subject ON t_subject.id_subject = t_login_subject.fk_subject_id
                                                        WHERE id_login = %(value_id_login_selected)s"""


        strsql_login_subject_non_attribues = """SELECT id_subject, name_subject FROM t_subject WHERE id_subject not in(SELECT id_subject as idLoginSubject FROM t_login_subject
                                                            INNER JOIN t_login ON t_login.id_login = t_login_subject.fk_login_id
                                                            INNER JOIN t_subject ON t_subject.id_subject = t_login_subject.fk_subject_id
                                                            WHERE id_login = %(value_id_login_selected)s)"""


        strsql_login_subject_attribues = """SELECT id_login, id_subject, name_subject FROM t_login_subject
                                                    INNER JOIN t_login ON t_login.id_login = t_login_subject.fk_login_id
                                                    INNER JOIN t_subject ON t_subject.id_subject = t_login_subject.fk_subject_id
                                                    WHERE id_login = %(value_id_login_selected)s"""

        # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
        with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
            print("jjjjjjjjjj")
            # Envoi de la commande MySql
            mc_afficher.execute(strsql_login_subject_non_attribues, valeur_id_login_selected_dict)
            # Récupère les données de la requête.
            data_login_subject_non_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("login_subject_afficher_data ----> data_login_subject_non_attribues ", data_login_subject_non_attribues,
                  " Type : ",
                  type(data_login_subject_non_attribues))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_subject_selected, valeur_id_login_selected_dict)
            # Récupère les données de la requête.
            data_login_selected = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_login_selected  ", data_login_selected, " Type : ", type(data_login_selected))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_login_subject_attribues, valeur_id_login_selected_dict)
            # Récupère les données de la requête.
            data_login_subject_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_login_subject_attribues ", data_login_subject_attribues, " Type : ",
                  type(data_login_subject_attribues))

            # Retourne les données des "SELECT"
            return data_login_selected, data_login_subject_non_attribues, data_login_subject_attribues
    except pymysql.Error as pymysql_erreur:
        code, msg = pymysql_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.Error Erreur dans login_subject_afficher_data : {sys.exc_info()[0]} "
              f"{pymysql_erreur.args[0]} , "
              f"{pymysql_erreur}", "danger")
    except Exception as exception_erreur:
        code, msg = exception_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"Exception Erreur dans login_subject_afficher_data : {sys.exc_info()[0]} "
              f"{exception_erreur.args[0]} , "
              f"{exception_erreur}", "danger")
    except pymysql.err.IntegrityError as IntegrityError_login_subject_afficher_data:
        code, msg = IntegrityError_login_subject_afficher_data.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.err.IntegrityError Erreur dans login_subject_afficher_data : {sys.exc_info()[0]} "
              f"{IntegrityError_login_subject_afficher_data.args[0]} , "
              f"{IntegrityError_login_subject_afficher_data}", "danger")
