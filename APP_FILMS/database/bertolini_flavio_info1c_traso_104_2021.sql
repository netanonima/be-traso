-- OM 2021.02.17
-- FICHIER MYSQL POUR FAIRE FONCTIONNER LES EXEMPLES
-- DE REQUETES MYSQL
-- Database: bertolini_flavio_info1c_traso_104_2021

-- Détection si une autre base de donnée du même nom existe

DROP DATABASE IF EXISTS bertolini_flavio_info1c_traso_104_2021;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS bertolini_flavio_info1c_traso_104_2021;

-- Utilisation de cette base de donnée

USE bertolini_flavio_info1c_traso_104_2021;
-- --------------------------------------------------------

--
-- Structure de la table `bertolini_flavio_info1c_traso_104_2021`
--

-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mar. 08 juin 2021 à 10:53
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `bertolini_flavio_info1c_traso_104_2021`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_login`
--

DROP TABLE IF EXISTS `t_login`;
CREATE TABLE IF NOT EXISTS `t_login` (
  `id_login` int(11) NOT NULL AUTO_INCREMENT,
  `fk_structure_id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(256) NOT NULL,
  `email` varchar(254) NOT NULL,
  `grade` int(1) NOT NULL,
  PRIMARY KEY (`id_login`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`),
  KEY `fk_structure_id` (`fk_structure_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `t_login`
--

INSERT INTO `t_login` (`id_login`, `fk_structure_id`, `username`, `password`, `email`, `grade`) VALUES
(2, 3, 'test', '$argon2id$v=19$m=102400,t=2,p=8$BqFXSZWQ+W6GfnOpbZxb4g$VEqBdqecXMNmH0fz5qrDtA', 'etst@sadf.com', 1),
(16, 2, 'netanonima', '$argon2id$v=19$m=102400,t=2,p=8$WyYXJag1lTjWGrA8yGp6iA$yV8j7wtLemYwk4mbddCHkg', 'test@test.com', 2),
(17, 3, 'lilianhugo', 's8ZdKk4Yq6dGjCi', 'hugo@gmail.com', 1),
(19, 5, 'lilian', 'lilian1234', 'lilian.jullien@formation.orif.ch', 1),
(20, 5, 'lilianjullien', 'lilian1234', 'cdihehei@gmail.com', 1),
(22, 5, 'jullienlilian', 'lilian1234', 'lilianjullien2002@gmail.com', 1),
(25, 6, 'baechlerdamien', 'OrifInfo2019', 'damien.baechler@formation.orif.ch', 1),
(27, 5, 'lucas', 'haha1234', 'lucas@example.ch', 1),
(28, 5, 'vivelorif', '$argon2id$v=19$m=102400,t=2,p=8$vUEIeV1594h4Z+K4EIf3Xg$uoZi1k5m6abBMndqU1KkpQ', 'jamesbond@example.ch', 1),
(29, 6, 'jacotpascal', 'Password00', 'pascal.jacot@formation.orif.ch', 1),
(31, 5, 'jacpas', 'Password00', '1950jacotpascal@gmail.com', 1),
(33, 5, 'namaioua', 'Zanga12345', 'a@toto.ch', 1),
(34, 5, 'nama', 'a1234567890', 'rr@rr.com', 1),
(35, 5, 'namai', 'a12345678', 'ee@ee.ch', 1);

-- --------------------------------------------------------

--
-- Structure de la table `t_login_subject`
--

DROP TABLE IF EXISTS `t_login_subject`;
CREATE TABLE IF NOT EXISTS `t_login_subject` (
  `id_login_subject` int(11) NOT NULL AUTO_INCREMENT,
  `fk_login_id` int(11) NOT NULL,
  `fk_subject_id` int(11) NOT NULL,
  PRIMARY KEY (`id_login_subject`),
  KEY `fk_login_id` (`fk_login_id`),
  KEY `fk_subject_id` (`fk_subject_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `t_login_subject`
--

INSERT INTO `t_login_subject` (`id_login_subject`, `fk_login_id`, `fk_subject_id`) VALUES
(1, 16, 2),
(3, 2, 1),
(4, 2, 4),
(5, 16, 6),
(6, 16, 4),
(13, 22, 3),
(14, 22, 12),
(15, 25, 19),
(16, 28, 21),
(20, 31, 9),
(22, 31, 23),
(23, 25, 11);

-- --------------------------------------------------------

--
-- Structure de la table `t_structure`
--

DROP TABLE IF EXISTS `t_structure`;
CREATE TABLE IF NOT EXISTS `t_structure` (
  `id_structure` int(11) NOT NULL AUTO_INCREMENT,
  `name_structure` varchar(60) NOT NULL,
  `street_and_number` varchar(50) NOT NULL,
  `po_box_and_city` varchar(40) NOT NULL,
  `phone_number` int(16) NOT NULL,
  PRIMARY KEY (`id_structure`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `t_structure`
--

INSERT INTO `t_structure` (`id_structure`, `name_structure`, `street_and_number`, `po_box_and_city`, `phone_number`) VALUES
(1, 'École professionnelle supérieure de la société industrielle', 'Rue de Genève 63', '1002 Lausanne', 213165858),
(2, 'École professionnelle commerciale de Lausanne', 'Chemin de la Prairie 11', '1014 Lausanne', 213169500),
(3, 'Ecole primaire Forel Lavaux', 'Impasse du Cul de Sac 13', '9999 New Lausanne', 216548897),
(4, 'Hearc Gestion', 'Rue de la Gare 38', '2000 Neuchâtel', 219999999),
(5, 'cycle d\'orientation', 'monthey route de l\'europe ', '1870 monthey', 786547532),
(6, 'école professionnelle', 'rue de st germain 2', '1965 savièse', 271234567),
(8, 'EPSIC Lausanne', 'Rue de Genève 63', '1002 Lausanne', 666666666),
(10, 'Ecole secondaire', 'Rue de l\'inconnue', '1868 Collombey', 241234567),
(12, 'cycle du vieux moulin', 'rue du vieux moulin 8', '1950 sion', 273233585),
(14, 'totoii', 'midi 1022', '1860 Aigle', 567778899);

-- --------------------------------------------------------

--
-- Structure de la table `t_subject`
--

DROP TABLE IF EXISTS `t_subject`;
CREATE TABLE IF NOT EXISTS `t_subject` (
  `id_subject` int(11) NOT NULL AUTO_INCREMENT,
  `name_subject` varchar(40) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id_subject`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `t_subject`
--

INSERT INTO `t_subject` (`id_subject`, `name_subject`) VALUES
(1, 'Français'),
(2, 'Anglais'),
(3, 'Mathématiques'),
(4, 'Sciences naturelles'),
(5, 'Finance'),
(6, 'Statistique'),
(7, 'Comptabilité'),
(8, 'Micro-économie'),
(9, 'Coloriage'),
(10, 'Chant'),
(11, 'Développement C sharp'),
(12, 'Développement Java'),
(13, 'Traitement de bases de données'),
(14, 'Culture générale'),
(15, 'rouiller'),
(17, 'jullien'),
(19, 'baechler'),
(21, 'rouillermonay'),
(23, 'pascal jacot'),
(25, 'amaioua');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `t_login`
--
ALTER TABLE `t_login`
  ADD CONSTRAINT `t_login_ibfk_1` FOREIGN KEY (`fk_structure_id`) REFERENCES `t_structure` (`id_structure`);

--
-- Contraintes pour la table `t_login_subject`
--
ALTER TABLE `t_login_subject`
  ADD CONSTRAINT `t_login_subject_ibfk_1` FOREIGN KEY (`fk_login_id`) REFERENCES `t_login` (`id_login`),
  ADD CONSTRAINT `t_login_subject_ibfk_2` FOREIGN KEY (`fk_subject_id`) REFERENCES `t_subject` (`id_subject`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
