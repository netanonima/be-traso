"""
    Fichier : gestion_structure_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les structure.
"""
import re
import sys

import flask_argon2
from flask import flash, Flask
from flask import render_template
from flask import request
from flask import url_for
from flask import redirect
from flask_argon2 import Argon2
from flask import session

app = Flask(__name__)
argon2 = Argon2(app)

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.erreurs.msg_erreurs import *
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.structure.gestion_structure_wtf_forms import FormWTFAjouterStructure
from APP_FILMS.structure.gestion_structure_wtf_forms import FormWTFDeleteStructure
from APP_FILMS.structure.gestion_structure_wtf_forms import FormWTFUpdateStructure
"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /structure_afficher
    
    Test : ex : http://127.0.0.1:5005/structure_afficher
    
    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                id_structure_sel = 0 >> tous les structure.
                id_structure_sel = "n" affiche le structure dont l'id est "n"
"""


@obj_mon_application.route("/structure_afficher/<string:order_by>/<int:id_structure_sel>", methods=['GET', 'POST'])
def structure_afficher(order_by, id_structure_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion structure ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestionstructure {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and id_structure_sel == 0:
                    strsql_structure_afficher = """SELECT id_structure, name_structure, street_and_number, po_box_and_city, phone_number FROM t_structure ORDER BY name_structure ASC"""
                    mc_afficher.execute(strsql_structure_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_structure"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du structure sélectionné avec un nom de variable
                    valeur_id_structure_selected_dictionnaire = {"value_id_structure_selected": id_structure_sel}
                    strsql_structure_afficher = """SELECT id_structure, name_structure, street_and_number, po_box_and_city, phone_number FROM t_structure WHERE id_structure = %(value_id_structure_selected)s"""

                    mc_afficher.execute(strsql_structure_afficher, valeur_id_structure_selected_dictionnaire)
                else:
                    strsql_structure_afficher = """SELECT * FROM t_structure ORDER BY id_structure DESC"""

                    mc_afficher.execute(strsql_structure_afficher)

                data_structure = mc_afficher.fetchall()

                print("data_structure ", data_structure, " Type : ", type(data_structure))

                # Différencier les messages si la table est vide.
                if not data_structure and id_structure_sel == 0:
                    flash("""La table "t_structure" est vide. !!""", "warning")
                elif not data_structure and id_structure_sel > 0:
                    # Si l'utilisateur change l'id_structure dans l'URL et que le structure n'existe pas,
                    flash(f"La structure demandée n'existe pas !!", "warning")
                # else:
                    # Dans tous les autres cas, c'est que la table "t_structure" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    # flash(f"Données structure affichées !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur}")
            raise Exception(f"RGG Erreur générale. {erreur}")
            raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("structure/structure_afficher.html", data=data_structure)

@obj_mon_application.route("/structure_ajouter", methods=['GET', 'POST'])
def structure_ajouter_wtf():
    form = FormWTFAjouterStructure()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion structure ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestionstructure {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():

                name_structure = form.nom_structure_wtf.data
                street_and_number = form.nom_street_and_number_wtf.data
                po_box_and_city = form.nom_box_and_city_wtf.data
                phone_number = form.nom_phone_number_wtf.data

                valeurs_insertion_dictionnaire = {"value_name_structure": name_structure,"value_street_and_number": street_and_number, "value_po_box_and_city": po_box_and_city, "value_phone_number": phone_number}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_structure = """INSERT INTO t_structure (name_structure, street_and_number, po_box_and_city, phone_number) VALUES (%(value_name_structure)s,%(value_street_and_number)s,%(value_po_box_and_city)s,%(value_phone_number)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_structure, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('structure_afficher', order_by='DESC', id_structure_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_structure_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_structure_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion structure CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("structure/structure_ajouter_wtf.html", form=form)



@obj_mon_application.route("/structure_update", methods=['GET', 'POST'])
def structure_update_wtf():
    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_structure"
    id_structure_update = request.values['id_structure_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateStructure()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "structure_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.

            name_structure = form_update.nom_structure_wtf_update.data
            street_and_number = form_update.nom_street_and_number_wtf_update.data
            po_box_and_city = form_update.nom_box_and_city_wtf_update.data
            phone_number = form_update.nom_phone_number_wtf_update.data

            valeur_update_dictionnaire = {"value_structure_id": id_structure_update,
                                              "value_name_structure": name_structure,
                                              "value_street_and_number": street_and_number,
                                              "value_po_box_and_city": po_box_and_city,
                                              "value_phone_number": phone_number}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_intitulestructure = """UPDATE t_structure SET name_structure = %(value_name_structure)s, street_and_number = %(value_street_and_number)s, po_box_and_city = %(value_po_box_and_city)s, phone_number = %(value_phone_number)s WHERE id_structure = %(value_structure_id)s"""

            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intitulestructure, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_structure_update"
            return redirect(url_for('structure_afficher', order_by="ASC", id_structure_sel=id_structure_update))
        elif request.method == "GET":

            # Opération sur la BD pour récupérer "id_structure" et "intitule_structure" de la "t_structure"
            str_sql_id_structure = "SELECT name_structure, street_and_number, po_box_and_city, phone_number FROM t_structure WHERE id_structure = %(value_structure_id)s"
            valeur_select_dictionnaire = {"value_structure_id": id_structure_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_structure, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom structure" pour l'UPDATE
            data_structure = mybd_curseur.fetchone()

            # Afficher la valeur sélectionnée dans le champ du formulaire "structure_update_wtf.html"
            form_update.nom_structure_wtf_update.data = data_structure["name_structure"]
            form_update.nom_street_and_number_wtf_update.data = data_structure["street_and_number"]
            form_update.nom_box_and_city_wtf_update.data = data_structure["po_box_and_city"]
            form_update.nom_phone_number_wtf_update.data = data_structure["phone_number"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans structure_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans structure_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans structure_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans structure_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("structure/structure_update_wtf.html", form_update=form_update)



@obj_mon_application.route("/structure_delete", methods=['GET', 'POST'])
def structure_delete_wtf():
    data_films_attribue_structure_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_structure"
    id_structure_delete = request.values['id_structure_btn_delete_html']

    # Objet formulaire pour effacer le structure sélectionné.
    form_delete = FormWTFDeleteStructure()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("structure_afficher", order_by="ASC", id_structure_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "structure/structure_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_films_attribue_structure_delete = session['data_films_attribue_structure_delete']
                print("data_films_attribue_structure_delete ", data_films_attribue_structure_delete)

                flash(f"Effacer le structure de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer structure" qui va irrémédiablement EFFACER le structure
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_structure": id_structure_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_films_structure = """DELETE FROM t_login WHERE fk_structure_id = %(value_id_structure)s"""
                str_sql_delete_idstructure = """DELETE FROM t_structure WHERE id_structure = %(value_id_structure)s"""
                # Manière brutale d'effacer d'abord la "fk_structure", même si elle n'existe pas dans la "t_structure_film"
                # Ensuite on peut effacer le structure vu qu'il n'est plus "lié" (INNODB) dans la "t_structure_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_films_structure, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_idstructure, valeur_delete_dictionnaire)

                flash(f"Utilisateur définitivement effacé !!", "success")
                print(f"Utilisateur définitivement effacé !!")

                # afficher les données
                return redirect(url_for('structure_afficher', order_by="ASC", id_structure_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_structure": id_structure_delete}
            print(id_structure_delete, type(id_structure_delete))

            # Requête qui affiche tous les films qui ont le structure que l'utilisateur veut effacer
            str_sql_structure_films_delete = """SELECT * FROM t_login 
                                            WHERE fk_structure_id = %(value_id_structure)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_structure_films_delete, valeur_select_dictionnaire)
            data_films_attribue_structure_delete = mybd_curseur.fetchall()
            print("data_films_attribue_structure_delete...", data_films_attribue_structure_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "structure/structure_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_films_attribue_structure_delete'] = data_films_attribue_structure_delete

            # Opération sur la BD pour récupérer "id_structure" et "intitule_structure" de la "t_structure"
            str_sql_id_structure = "SELECT * FROM t_structure WHERE id_structure = %(value_id_structure)s"

            mybd_curseur.execute(str_sql_id_structure, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom structure" pour l'action DELETE
            data_nom_structure = mybd_curseur.fetchone()

            # Afficher la valeur sélectionnée dans le champ du formulaire "structure_delete_wtf.html"
            form_delete.nom_structure_delete_wtf.data = data_nom_structure["name_structure"]

            # Le bouton pour l'action "DELETE" dans le form. "structure_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans structure_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans structure_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans structure_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans structure_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("structure/structure_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_films_associes=data_films_attribue_structure_delete)
