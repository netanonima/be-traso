
from flask_wtf import FlaskForm
from wtforms import SelectField
from wtforms import StringField
from wtforms import SubmitField
from wtforms import PasswordField
from wtforms import validators
from wtforms.validators import DataRequired
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterStructure(FlaskForm):
    nom_structure_regexp = "^[a-zA-ZÀ-ÖØ-öø-ÿ0-9 ']*$"
    nom_structure_wtf = StringField("Saisir le nom du centre de formation ",
                                      validators=[Length(min=6, max=60, message="min 6 max 60"),
                                                  validators.DataRequired(),
                                                  Regexp(nom_structure_regexp,
                                                         message="Seulement lettres minuscules et majuscules"
                                                                 "lettres accentuées, nombre,"
                                                                 "espaces et apostrophes")
                                                  ])
    nom_street_and_number_regexp = "^[a-zA-ZÀ-ÖØ-öø-ÿ0-9 ']*$"
    nom_street_and_number_wtf = StringField("Saisir la rue et le numéro de rue ", validators=[Length(min=6, max=50, message="min 6 max 50"),
                                                                       validators.DataRequired(),
                                                                       Regexp(nom_street_and_number_regexp,
                                                         message="Seulement lettres minuscules et majuscules"
                                                                 "lettres accentuées, nombre,"
                                                                 "espaces et apostrophes")
                                                                       ])

    nom_po_box_and_city_regexp = "^[a-zA-ZÀ-ÖØ-öø-ÿ0-9 ']*$"
    nom_box_and_city_wtf = StringField("Saisir npa et la ville ",
                                              validators=[Length(min=6, max=40, message="min 6 max 40"),
                                                          validators.DataRequired(),
                                                          Regexp(nom_po_box_and_city_regexp,
                                                                 message="Seulement lettres minuscules et majuscules"
                                                                         "lettres accentuées, nombre,"
                                                                         "espaces et apostrophes")
                                                          ])

    nom_phone_number_regexp = "^[0-9]+$"
    nom_phone_number_wtf = StringField("Saisir le numéro de téléphone ",
                                              validators=[Length(min=9, max=16, message="min 9 max 16"),
                                                          validators.DataRequired(),
                                                          Regexp(nom_phone_number_regexp,
                                                                 message="Seulement chiffres")
                                                          ])

    submit = SubmitField("Enregistrer le centre de formation")


class FormWTFUpdateStructure(FlaskForm):
    nom_structure_regexp_update = "^[a-zA-ZÀ-ÖØ-öø-ÿ0-9 ']*$"
    nom_structure_wtf_update = StringField("Saisir le nom du centre de formation ",
                                      validators=[Length(min=6, max=60, message="min 6 max 60"),
                                                  validators.DataRequired(),
                                                  Regexp(nom_structure_regexp_update,
                                                         message="Seulement lettres minuscules et majuscules"
                                                                 "lettres accentuées, nombre,"
                                                                 "espaces et apostrophes")
                                                  ])
    nom_street_and_number_regexp_update = "^[a-zA-ZÀ-ÖØ-öø-ÿ0-9 ']*$"
    nom_street_and_number_wtf_update = StringField("Saisir la rue et le numéro de rue ", validators=[Length(min=6, max=50, message="min 6 max 50"),
                                                                       validators.DataRequired(),
                                                                       Regexp(nom_street_and_number_regexp_update,
                                                         message="Seulement lettres minuscules et majuscules"
                                                                 "lettres accentuées, nombre,"
                                                                 "espaces et apostrophes")
                                                                       ])

    nom_po_box_and_city_regexp_update = "^[a-zA-ZÀ-ÖØ-öø-ÿ0-9 ']*$"
    nom_box_and_city_wtf_update = StringField("Saisir npa et la ville ",
                                              validators=[Length(min=6, max=40, message="min 6 max 40"),
                                                          validators.DataRequired(),
                                                          Regexp(nom_po_box_and_city_regexp_update,
                                                                 message="Seulement lettres minuscules et majuscules"
                                                                         "lettres accentuées, nombre,"
                                                                         "espaces et apostrophes")
                                                          ])

    nom_phone_number_regexp_update = "^[0-9]+$"
    nom_phone_number_wtf_update = StringField("Saisir le numéro de téléphone ",
                                              validators=[Length(min=9, max=16, message="min 9 max 16"),
                                                          validators.DataRequired(),
                                                          Regexp(nom_phone_number_regexp_update,
                                                                 message="Seulement chiffres")
                                                          ])

    submit = SubmitField("Mettre à jour le centre de formation")


class FormWTFDeleteStructure(FlaskForm):
    """
        Dans le formulaire "genre_delete_wtf.html"

        nom_genre_delete_wtf : Champ qui reçoit la valeur du genre, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "genre".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_genre".
    """
    nom_structure_delete_wtf = StringField("Effacer cette structure")
    submit_btn_del = SubmitField("Effacer structure")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
