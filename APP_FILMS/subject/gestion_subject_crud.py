"""
    Fichier : gestion_subject_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les subject.
"""
import re
import sys

import flask_argon2
from flask import flash, Flask
from flask import render_template
from flask import request
from flask import url_for
from flask import redirect
from flask_argon2 import Argon2
from flask import session

app = Flask(__name__)
argon2 = Argon2(app)

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.erreurs.msg_erreurs import *
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.subject.gestion_subject_wtf_forms import FormWTFAjouterSubject
from APP_FILMS.subject.gestion_subject_wtf_forms import FormWTFDeleteSubject
from APP_FILMS.subject.gestion_subject_wtf_forms import FormWTFUpdateSubject
"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /subject_afficher
    
    Test : ex : http://127.0.0.1:5005/subject_afficher
    
    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                id_subject_sel = 0 >> tous les subject.
                id_subject_sel = "n" affiche le subject dont l'id est "n"
"""


@obj_mon_application.route("/subject_afficher/<string:order_by>/<int:id_subject_sel>", methods=['GET', 'POST'])
def subject_afficher(order_by, id_subject_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion subject ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestionsubject {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and id_subject_sel == 0:
                    strsql_subject_afficher = """SELECT id_subject, name_subject FROM t_subject ORDER BY name_subject ASC"""
                    mc_afficher.execute(strsql_subject_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_subject"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du subject sélectionné avec un nom de variable
                    valeur_id_subject_selected_dictionnaire = {"value_id_subject_selected": id_subject_sel}
                    strsql_subject_afficher = """SELECT id_subject, name_subject FROM t_subject WHERE id_subject = %(value_id_subject_selected)s"""

                    mc_afficher.execute(strsql_subject_afficher, valeur_id_subject_selected_dictionnaire)
                else:
                    strsql_subject_afficher = """SELECT * FROM t_subject ORDER BY id_subject DESC"""

                    mc_afficher.execute(strsql_subject_afficher)

                data_subject = mc_afficher.fetchall()

                print("data_subject ", data_subject, " Type : ", type(data_subject))

                # Différencier les messages si la table est vide.
                if not data_subject and id_subject_sel == 0:
                    flash("""La table "t_subject" est vide. !!""", "warning")
                elif not data_subject and id_subject_sel > 0:
                    # Si l'utilisateur change l'id_subject dans l'URL et que le subject n'existe pas,
                    flash(f"La subject demandée n'existe pas !!", "warning")
                # else:
                    # Dans tous les autres cas, c'est que la table "t_subject" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    # flash(f"Données subject affichées !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur}")
            raise Exception(f"RGG Erreur générale. {erreur}")
            raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("subject/subject_afficher.html", data=data_subject)

@obj_mon_application.route("/subject_ajouter", methods=['GET', 'POST'])
def subject_ajouter_wtf():
    form = FormWTFAjouterSubject()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion subject ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestionsubject {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():

                name_subject = form.nom_subject_wtf.data

                valeurs_insertion_dictionnaire = {"value_name_subject": name_subject}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_subject = """INSERT INTO t_subject (name_subject) VALUES (%(value_name_subject)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_subject, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('subject_afficher', order_by='DESC', id_subject_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_subject_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_subject_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion subject CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("subject/subject_ajouter_wtf.html", form=form)



@obj_mon_application.route("/subject_update", methods=['GET', 'POST'])
def subject_update_wtf():
    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_subject"
    id_subject_update = request.values['id_subject_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateSubject()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "subject_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.

            name_subject = form_update.nom_subject_wtf_update.data

            valeur_update_dictionnaire = {"value_subject_id": id_subject_update,
                                              "value_name_subject": name_subject}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_intitulesubject = """UPDATE t_subject SET name_subject = %(value_name_subject)s WHERE id_subject = %(value_subject_id)s"""

            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intitulesubject, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_subject_update"
            return redirect(url_for('subject_afficher', order_by="ASC", id_subject_sel=id_subject_update))
        elif request.method == "GET":

            # Opération sur la BD pour récupérer "id_subject" et "intitule_subject" de la "t_subject"
            str_sql_id_subject = "SELECT name_subject FROM t_subject WHERE id_subject = %(value_subject_id)s"
            valeur_select_dictionnaire = {"value_subject_id": id_subject_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_subject, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom subject" pour l'UPDATE
            data_subject = mybd_curseur.fetchone()

            # Afficher la valeur sélectionnée dans le champ du formulaire "subject_update_wtf.html"
            form_update.nom_subject_wtf_update.data = data_subject["name_subject"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans subject_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans subject_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans subject_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans subject_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("subject/subject_update_wtf.html", form_update=form_update)



@obj_mon_application.route("/subject_delete", methods=['GET', 'POST'])
def subject_delete_wtf():
    data_films_attribue_subject_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_subject"
    id_subject_delete = request.values['id_subject_btn_delete_html']

    # Objet formulaire pour effacer le subject sélectionné.
    form_delete = FormWTFDeleteSubject()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("subject_afficher", order_by="ASC", id_subject_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "subject/subject_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_films_attribue_subject_delete = session['data_films_attribue_subject_delete']
                print("data_films_attribue_subject_delete ", data_films_attribue_subject_delete)

                flash(f"Effacer le subject de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer subject" qui va irrémédiablement EFFACER le subject
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_subject": id_subject_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_films_subject = """DELETE FROM t_login_subject WHERE fk_subject_id = %(value_id_subject)s"""
                str_sql_delete_idsubject = """DELETE FROM t_subject WHERE id_subject = %(value_id_subject)s"""
                # Manière brutale d'effacer d'abord la "fk_subject", même si elle n'existe pas dans la "t_subject_film"
                # Ensuite on peut effacer le subject vu qu'il n'est plus "lié" (INNODB) dans la "t_subject_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_films_subject, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_idsubject, valeur_delete_dictionnaire)

                flash(f"Branche définitivement effacée !!", "success")
                print(f"Branche définitivement effacée !!")

                # afficher les données
                return redirect(url_for('subject_afficher', order_by="ASC", id_subject_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_subject": id_subject_delete}
            print(id_subject_delete, type(id_subject_delete))

            # Requête qui affiche tous les films qui ont le subject que l'utilisateur veut effacer
            str_sql_subject_films_delete = """SELECT * FROM t_login_subject 
                                            WHERE fk_subject_id = %(value_id_subject)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_subject_films_delete, valeur_select_dictionnaire)
            data_films_attribue_subject_delete = mybd_curseur.fetchall()
            print("data_films_attribue_subject_delete...", data_films_attribue_subject_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "subject/subject_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_films_attribue_subject_delete'] = data_films_attribue_subject_delete

            # Opération sur la BD pour récupérer "id_subject" et "intitule_subject" de la "t_subject"
            str_sql_id_subject = "SELECT * FROM t_subject WHERE id_subject = %(value_id_subject)s"

            mybd_curseur.execute(str_sql_id_subject, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom subject" pour l'action DELETE
            data_nom_subject = mybd_curseur.fetchone()

            # Afficher la valeur sélectionnée dans le champ du formulaire "subject_delete_wtf.html"
            form_delete.nom_subject_delete_wtf.data = data_nom_subject["name_subject"]

            # Le bouton pour l'action "DELETE" dans le form. "subject_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans subject_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans subject_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans subject_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans subject_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("subject/subject_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_films_associes=data_films_attribue_subject_delete)
