
from flask_wtf import FlaskForm
from wtforms import SelectField
from wtforms import StringField
from wtforms import SubmitField
from wtforms import PasswordField
from wtforms import validators
from wtforms.validators import DataRequired
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterSubject(FlaskForm):
    nom_subject_regexp = "^[a-zA-ZÀ-ÖØ-öø-ÿ0-9 ']*$"
    nom_subject_wtf = StringField("Saisir le nom du centre de formation ",
                                      validators=[Length(min=4, max=40, message="min 4 max 40"),
                                                  validators.DataRequired(),
                                                  Regexp(nom_subject_regexp,
                                                         message="Seulement lettres minuscules et majuscules"
                                                                 "lettres accentuées, nombre,"
                                                                 "espaces et apostrophes")
                                                  ])

    submit = SubmitField("Enregistrer la branche")


class FormWTFUpdateSubject(FlaskForm):
    nom_subject_regexp_update = "^[a-zA-ZÀ-ÖØ-öø-ÿ0-9 ']*$"
    nom_subject_wtf_update = StringField("Saisir le nom du centre de formation ",
                                      validators=[Length(min=4, max=40, message="min 4 max 40"),
                                                  validators.DataRequired(),
                                                  Regexp(nom_subject_regexp_update,
                                                         message="Seulement lettres minuscules et majuscules"
                                                                 "lettres accentuées, nombre,"
                                                                 "espaces et apostrophes")
                                                  ])

    submit = SubmitField("Mettre à jour la branche")


class FormWTFDeleteSubject(FlaskForm):
    nom_subject_delete_wtf = StringField("Effacer cette branche")
    submit_btn_del = SubmitField("Effacer branche")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
